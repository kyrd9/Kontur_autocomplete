import { getCities } from '../lib/api'

export default {
  state: {
    cities: [],
    hasError: [false],
    selected: [false]
  },
  actions: {
    CitiesList ({ commit }) {
      setTimeout(() => {
        getCities().then((response) => {
          commit('CitiesList', response)
        })
      }, 500)
    }
  },
  mutations: {
    changeForm (state, hasError) {
      state.hasError.splice(0, 1)
      state.hasError.push(hasError[0])
      state.selected.splice(0, 1)
      state.selected.push(hasError[1])
    },
    CitiesList (state, CitiesList) {
      if (state.cities.length === 0) {
        state.cities.splice(0, CitiesList.length)
        state.cities.push(...CitiesList)
      }
    }
  }
}
