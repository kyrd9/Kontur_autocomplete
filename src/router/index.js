import Vue from 'vue'
import Router from 'vue-router'
import Homepage from '@/container/Homepage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'autocomplete',
      component: Homepage
    }
  ]
})
